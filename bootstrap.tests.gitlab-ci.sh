#!/usr/bin/env bash

import "test-import-in-remote-mode"
include "tests/test-include-in-remote-mode.sh"

# Test multiple imports and includes.
import "test-import-in-remote-mode"
include "tests/test-include-in-remote-mode.sh"

import "test-import-with-file" "tests/test-import-with-file.gitlab-ci.sh"

include "tests/test-hooks-in-remote-mode.sh"
include "tests/test-path-in-remote-mode.sh"

################################################################################
## FUNCTIONS
################################################################################

function test_echo_task() {
    echo_gray "This is a gray message."
    echo_red "This is a red message."
    echo_green "This is a green message."
    echo_yellow "This is a yellow message."
    echo_blue "This is a blue message."
    echo_magenta "This is a magenta message."
    echo_cyan "This is a cyan message."
    echo

    echo_error "This is a short error message."
    echo_error \
        "This is a long" \
        "error message"
    echo

    echo_warning "This is a short warning message."
    echo_warning \
        "This is a long" \
        "warning message"
    echo
}

function test_import_in_remote_mode_task() {
    test_import_in_remote_mode

    echo "TEST_IMPORT_IN_REMOTE_MODE_LOADED: $TEST_IMPORT_IN_REMOTE_MODE_LOADED"
    echo "TEST_IMPORT_IN_REMOTE_MODE_CALLED: $TEST_IMPORT_IN_REMOTE_MODE_CALLED"
    echo

    [ "$TEST_IMPORT_IN_REMOTE_MODE_LOADED" == 1 ] || exit 1
    [ "$TEST_IMPORT_IN_REMOTE_MODE_CALLED" == 1 ] || exit 1
}

function test_import_in_local_mode_task() {
    test_import_in_local_mode

    echo "TEST_IMPORT_IN_LOCAL_MODE_LOADED: $TEST_IMPORT_IN_LOCAL_MODE_LOADED"
    echo "TEST_IMPORT_IN_LOCAL_MODE_CALLED: $TEST_IMPORT_IN_LOCAL_MODE_CALLED"
    echo

    [ "$TEST_IMPORT_IN_LOCAL_MODE_LOADED" == "1" ] || exit 1
    [ "$TEST_IMPORT_IN_LOCAL_MODE_CALLED" == "1" ] || exit 1
}

function test_import_with_file_task() {
    test_import_with_file

    echo "TEST_IMPORT_WITH_FILE_LOADED: $TEST_IMPORT_WITH_FILE_LOADED"
    echo "TEST_IMPORT_WITH_FILE_CALLED: $TEST_IMPORT_WITH_FILE_CALLED"
    echo

    [ "$TEST_IMPORT_WITH_FILE_LOADED" == "1" ] || exit 1
    [ "$TEST_IMPORT_WITH_FILE_CALLED" == "1" ] || exit 1
}

function test_include_in_remote_mode_task() {
    test_include_in_remote_mode

    echo "TEST_INCLUDE_IN_REMOTE_MODE_LOADED: $TEST_INCLUDE_IN_REMOTE_MODE_LOADED"
    echo "TEST_INCLUDE_IN_REMOTE_MODE_CALLED: $TEST_INCLUDE_IN_REMOTE_MODE_CALLED"
    echo

    [ "$TEST_INCLUDE_IN_REMOTE_MODE_LOADED" == "1" ] || exit 1
    [ "$TEST_INCLUDE_IN_REMOTE_MODE_CALLED" == "1" ] || exit 1
}

function test_include_in_local_mode_task() {
    test_include_in_local_mode

    echo "TEST_INCLUDE_IN_LOCAL_MODE_LOADED: $TEST_INCLUDE_IN_LOCAL_MODE_LOADED"
    echo "TEST_INCLUDE_IN_LOCAL_MODE_CALLED: $TEST_INCLUDE_IN_LOCAL_MODE_CALLED"
    echo

    [ "$TEST_INCLUDE_IN_LOCAL_MODE_LOADED" == "1" ] || exit 1
    [ "$TEST_INCLUDE_IN_LOCAL_MODE_CALLED" == "1" ] || exit 1
}

function test_before_hooks_task() {
    echo "TEST_BEFORE_HOOK_IN_REMOTE_MODE_CALLED: $TEST_BEFORE_HOOK_IN_REMOTE_MODE_CALLED"
    echo "TEST_BEFORE_HOOK_IN_LOCAL_MODE_CALLED: $TEST_BEFORE_HOOK_IN_LOCAL_MODE_CALLED"
    echo

    [ "$TEST_BEFORE_HOOK_IN_REMOTE_MODE_CALLED" == "1" ] || exit 1
    [ "$TEST_BEFORE_HOOK_IN_LOCAL_MODE_CALLED" == "1" ] || exit 1

    echo "TEST_AFTER_HOOK_IN_REMOTE_MODE_CALLED: $TEST_AFTER_HOOK_IN_REMOTE_MODE_CALLED"
    echo "TEST_AFTER_HOOK_IN_LOCAL_MODE_CALLED: $TEST_AFTER_HOOK_IN_LOCAL_MODE_CALLED"
    echo

    [ -z "$TEST_AFTER_HOOK_IN_REMOTE_MODE_CALLED" ] || exit 1
    [ -z "$TEST_AFTER_HOOK_IN_LOCAL_MODE_CALLED" ] || exit 1
}

function test_after_hooks_task() {
    echo "TEST_AFTER_HOOK_IN_REMOTE_MODE_CALLED: $TEST_AFTER_HOOK_IN_REMOTE_MODE_CALLED"
    echo "TEST_AFTER_HOOK_IN_LOCAL_MODE_CALLED: $TEST_AFTER_HOOK_IN_LOCAL_MODE_CALLED"
    echo

    [ "$TEST_AFTER_HOOK_IN_REMOTE_MODE_CALLED" == "1" ] || exit 1
    [ "$TEST_AFTER_HOOK_IN_LOCAL_MODE_CALLED" == "1" ] || exit 1
}

function test_path_in_remote_mode_task() {
    test_path_in_remote_mode

    echo "TEST_PATH_IN_REMOTE_MODE_TEXT: $TEST_PATH_IN_REMOTE_MODE_TEXT"
    echo

    [ "$TEST_PATH_IN_REMOTE_MODE_TEXT" == "REMOTE" ] || exit 1
}

function test_path_in_local_mode_task() {
    test_path_in_local_mode

    echo "TEST_PATH_IN_LOCAL_MODE_TEXT: $TEST_PATH_IN_LOCAL_MODE_TEXT"
    echo

    [ "$TEST_PATH_IN_LOCAL_MODE_TEXT" == "LOCAL" ] || exit 1
}


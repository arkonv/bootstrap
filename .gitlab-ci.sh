#!/usr/bin/env bash

import "test-import-in-local-mode"
include "tests/test-include-in-local-mode.sh"

# Test multiple imports and includes.
import "test-import-in-local-mode"
include "tests/test-include-in-local-mode.sh"

include "tests/test-hooks-in-local-mode.sh"
include "tests/test-path-in-local-mode.sh"

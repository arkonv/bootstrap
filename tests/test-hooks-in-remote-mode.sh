#!/usr/bin/env bash

function before_test_before_hooks_task() {
    TEST_BEFORE_HOOK_IN_REMOTE_MODE_CALLED=$(($TEST_BEFORE_HOOK_IN_REMOTE_MODE_CALLED + 1))
}

function after_test_before_hooks_task() {
    TEST_AFTER_HOOK_IN_REMOTE_MODE_CALLED=$(($TEST_AFTER_HOOK_IN_REMOTE_MODE_CALLED + 1))
}

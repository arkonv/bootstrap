#!/usr/bin/env bash

TEST_INCLUDE_IN_LOCAL_MODE_LOADED=$(($TEST_INCLUDE_IN_LOCAL_MODE_LOADED + 1))

function test_include_in_local_mode() {
    TEST_INCLUDE_IN_LOCAL_MODE_CALLED=$(($TEST_INCLUDE_IN_LOCAL_MODE_CALLED + 1))
}

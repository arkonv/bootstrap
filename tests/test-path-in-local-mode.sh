#!/usr/bin/env bash

TEST_PATH_IN_LOCAL_MODE_TEXT=""

function test_path_in_local_mode() {
    TEST_PATH_IN_LOCAL_MODE_TEXT=$(cat "$(path "tests/test-path-in-local-mode.txt")")
}

# GitLab CI Bootstrap

GitLab CI Bootstrap - это микрофреймворк для разработки [GitLab CI](https://docs.gitlab.com/ee/ci/). Его основные цели:

- Разделить описание пайплайна и его скрипты (bash или shell), чтобы оставить файл `.gitlab-ci.yml` более деклоративным.
- Разбить большие монолитные скрипты на более мелкие, чтобы структурировать их и упростить поддержку.
- Вынести отдельные скрипты и джобы (с их скриптами) в отдельные репозитории, чтобы повторно использовать их в различных проектах.

## Подключение

GitLab CI Bootstrap состоит из одного единственного файла [`bootstrap.gitlab-ci.yml`](bootstrap.gitlab-ci.yml), который необходимо подключить в файле `.gitlab-ci.yml`, используя инструкцию [`include`](https://docs.gitlab.com/ee/ci/yaml/#include). Сделать это можно несколькими способами:

* Для GitLab SaaS подключить файл из этого репозитория используя [`include:file`](https://docs.gitlab.com/ee/ci/yaml/#includefile):

    ```yml
    include:
      - project: 'chakrygin/bootstrap'
        ref: 'master'
        file: 'bootstrap.gitlab-ci.yml'
    ```

* Для GitLab Self-Hosted подключить файл из этого репозитория используя [`include:remote`](https://docs.gitlab.com/ee/ci/yaml/#includeremote):

    ```yml
    include:
      - remote: 'https://gitlab.com/chakrygin/bootstrap/-/raw/master/bootstrap.gitlab-ci.yml'
    ```

* Скопировать файл в свой репозиторий и подключить его используя [`include:local`](https://docs.gitlab.com/ee/ci/yaml/#includelocal):

    ```yml
    include:
      - local: 'bootstrap.gitlab-ci.yml'
    ```

* Скопировать файл в свой отдельный репозиторий и подключить его используя [`include:file`](https://docs.gitlab.com/ee/ci/yaml/#includefile) или [`include:remote`](https://docs.gitlab.com/ee/ci/yaml/#includeremote), как описано выше.

Первые два способа можно использовать для быстрого старта. В остальных случаях рекомендуется иметь собственную копию файла.

## Использование

GitLab CI Bootstrap основан на том, что GitLab выполяет команды, указанные в [`before_script`](https://docs.gitlab.com/ee/ci/yaml/#before_script) и [`script`](https://docs.gitlab.com/ee/ci/yaml/#script) в одной и той же оболочке, поэтому все переменные и функции, объявленные в [`before_script`](https://docs.gitlab.com/ee/ci/yaml/#before_script) будут также доступны и в [`script`](https://docs.gitlab.com/ee/ci/yaml/#script).

Файл [`bootstrap.gitlab-ci.yml`](bootstrap.gitlab-ci.yml) содержит один единственный джоб `.bootstrap`, который отвечает за загрузку скриптов. Этот джоб имеет следующий вид:

```yml
.bootstrap:
  before_script:
    - '...'
```

Другие джобы могут расширять джоб `.bootstrap`, используя [`extends`](https://docs.gitlab.com/ee/ci/yaml/#extends), чтобы использовать переменные и функции, созданные на этапе загрузки скриптов:

```yml
example:
  extends:
    - '.bootstrap'
  script:
    - '...'
```

Загрузка скриптов может выполняться как из локального, так и из удалённого репозиториев.

(Здесь и далее под локальным будет пониматься репозиторий, в котором непосредственно запущен пайплайн.
И напротив, под удалённым будет пониматься любой другой репозиторий в том же экземпляре GitLab.)

## Загрузка скриптов из локального репозитория

Если в локальном репозитории существует файл `.gitlab-ci.sh`, то все джобы будут автоматически загружать его и смогут использовать функции и переменные, объявленные в этом файле.

### Пример

* Репозиторий `examples/hello`

    * Файл `.gitlab-ci.yml`

        ```yml
        include:
          - local: 'bootstrap.gitlab-ci.yml'

        hello:
          extends: '.bootstrap'
          script:
            - 'hello_task'
        ```

    * Файл `.gitlab-ci.sh`

        ```bash
        function hello_task() {
            echo "Hello, $GITLAB_USER_NAME!"
        }
        ```

## Загрузка скриптов из удалённого репозитория

Если в джобе определена переменная `CI_IMPORT`, то этот джоб проверит наличие трёх переменных: `CI_{module}_PROJECT`, `CI_{module}_REF` и `CI_{module}_FILE`, где `{module}` - это нормализованное значение переменной `CI_IMPORT` (приведённое в верхнему регистру с заменой дефисов на подчёркивания).

При наличии всех трёх переменных джоб будет автоматически загружать указанный файл из указанной ветки или тега указанного репозитория и сможет использовать функции и переменные, объявленные в этом файле.

### Пример

* Репозиторий `examples/hello`

    * Файл `.gitlab-ci.yml`

        ```yml
        include:
          - project: 'examples/hello-ci'
            ref: 'master'
            file: 'hello.gitlab-ci.yml'
        ```

* Репозиторий `examples/hello-ci`

    * Файл `hello.gitlab-ci.yml`

        ```yml
        include:
          - local: 'bootstrap.gitlab-ci.yml'

        variables:
            CI_HELLO_PROJECT: 'examples/hello-ci'
            CI_HELLO_REF: 'master'
            CI_HELLO_FILE: 'hello.gitlab-ci.sh'

        hello:
          extends: '.bootstrap'
          variables:
            CI_IMPORT: 'hello'
          script:
            - 'hello_task'
        ```

    * Файл `hello.gitlab-ci.sh`

        ```bash
        function hello_task() {
            echo "Hello, $GITLAB_USER_NAME!"
        }
        ```

### Замечания

* Загрузка скриптов из удалённого репозитория всегда производится **до** загрузки скриптов из локального репозитория.

* Загрузка скриптов из удалённого репозитория требует, чтобы значение переменной `CI_{module}_REF` всегда совпадало с названием ветки или тега, где расположен подключаемый yml-файл, загружающий скрипты. Иначе будет загружена неправильная версия скриптов. Обеспечить это можно несколькими способами:
    * Использовать [Git Hooks](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks) и исправлять значение переменной перед каждым коммитом локально.
    * Использовать GitLab CI и исправлять значение переменной после каждого коммита в специальном пайплайне.

## Функция `include`

Функция `include` позволяет разбивать большие скрипты на более мелкие и подключать одни скрипты к другим.

Если в процессе загрузки скрипта в нём встречается вызов функции `include`, файл, указанный в аргументе этой функции также будет загружен. При этом не важно, из какого репозитория в данный момент загружается текущий скрипт, локального или удалённого. Функция `include` всегда загружет файл из того же репозитория, из которого она вызвана.

### Пример

* Репозиторий `examples/hello`

    * Файл `.gitlab-ci.yml`

        ```yml
        include:
          - project: 'examples/hello-ci'
            ref: 'master'
            file: 'hello.gitlab-ci.yml'
        ```

* Репозиторий `examples/hello-ci`

    * Файл `hello.gitlab-ci.yml`

        ```yml
        include:
          - local: 'bootstrap.gitlab-ci.yml'

        variables:
            CI_HELLO_PROJECT: 'examples/hello-ci'
            CI_HELLO_REF: 'master'
            CI_HELLO_FILE: 'hello.gitlab-ci.sh'

        hello:
          extends: '.bootstrap'
          variables:
            CI_IMPORT: 'hello'
          script:
            - 'hello_task'
        ```

    * Файл `hello.gitlab-ci.sh`

        ```bash
        include "ci/functions/greet.sh"

        function hello_task() {
            greet "$GITLAB_USER_NAME"
        }
        ```

    * Файл `ci/functions/greet.sh`

        ```bash
        function greet() {
            local hour=$(date +"%H")

            if [ $hour -ge 5 ] && [ $hour -lt 12 ]; then
                echo "Good morning, $1!"
            elif [ $hour -ge 12 ] && [ $hour -lt 18 ]; then
                echo "Good afternoon, $1!"
            elif [ $hour -ge 18 ] && [ $hour -lt 22 ]; then
                echo "Good evening, $1!"
            else
                echo "Good night, $1!"
            fi
        }
        ```

## Функция `import`

Функция `import` позволяет выность скрипты в отдельный репозиторий и подключать их в другом репозитории в виде модулей.

Если в процессе загрузки скрипта в нём встречается вызов функции `import`, проверяется наличие трёх переменных: `CI_{module}_PROJECT`, `CI_{module}_REF` и `CI_{module}_FILE`, где `{module}` - это нормализованное значение аргумента этой функции (приведённое в верхнему регистру с заменой дефисов на подчёркивания). При наличии всех трёх переменных указанный файл также будет загружен.

### Пример

* Репозиторий `examples/hello`

    * Файл `.gitlab-ci.yml`

        ```yml
        include:
          - project: 'examples/hello-ci'
            ref: 'master'
            file: 'hello.gitlab-ci.yml'
        ```

* Репозиторий `examples/hello-ci`

    * Файл `hello.gitlab-ci.yml`

        ```yml
        include:
          - local: 'bootstrap.gitlab-ci.yml'

          - project: 'examples/greet-ci'
            ref: 'master'
            file: 'greet.gitlab-ci.yml'

        variables:
            CI_HELLO_PROJECT: 'examples/hello-ci'
            CI_HELLO_REF: 'master'
            CI_HELLO_FILE: 'hello.gitlab-ci.sh'

        hello:
          extends: '.bootstrap'
          variables:
            CI_IMPORT: 'hello'
          script:
            - 'hello_task'
        ```

    * Файл `hello.gitlab-ci.sh`

        ```bash
        import "greet"

        function hello_task() {
            greet "$GITLAB_USER_NAME"
        }
        ```

* Репозиторий `examples/greet-ci`

    * Файл `greet.gitlab-ci.yml`

        ```yml
        variables:
            CI_GREET_PROJECT: 'examples/greet-ci'
            CI_GREET_REF: 'master'
            CI_GREET_FILE: 'greet.gitlab-ci.sh'
        ```

    * Файл `greet.gitlab-ci.sh`

        ```bash
        function greet() {
            local hour=$(date +"%H")

            if [ $hour -ge 5 ] && [ $hour -lt 12 ]; then
                echo "Good morning, $1!"
            elif [ $hour -ge 12 ] && [ $hour -lt 18 ]; then
                echo "Good afternoon, $1!"
            elif [ $hour -ge 18 ] && [ $hour -lt 22 ]; then
                echo "Good evening, $1!"
            else
                echo "Good night, $1!"
            fi
        }
        ```
